function Rammy() {
	this.pageTitle = "A Rammy Application";
	
	this.START = function(imports) {
		document.write("<head></head><body> </body>");
		document.head.innerHTML = '<meta charset="UTF-8"/>'
								+ '<title>'
								+ this.pageTitle
								+ '</title>';
		for(file in imports) {
			document.head.innerHTML = document.head.innerHTML + '<script src="' + imports[file] + '"></script>';
		}
	}
	
	this.setTitle = function(title) {
		this.pageTitle = title;
		document.title = this.pageTitle;
	},

	this.use = function(filename) {
		var html, done = true;
		if(filename.indexOf(".js") >= 1) {
			html = '<script src="' + filename + '" type="text/javascript"></script>';
		} else if(filename.indexOf(".css") >= 1) {
			html = '<link rel="stylesheet" type="text/css" href="' + filename + '"/>';
		} else {
			console.log("File does not exist in method [...].use('" + filename + "')");
			done = false
		}
		if(done) {
			var head = document.head.innerHTML;
			head = head + html;
			document.head.innerHTML = head;
		}
	},

	this.getTitle = function() {
		return document.title;
	}

	this.LABEL = function() {
		return [document.createElement("SPAN"), "label"];
	}

	this.BUTTON = function() {
		return [document.createElement("BUTTON"), "button"];
	}

	this.SECTION = function() {
		return [document.createElement("DIV"), "section"];
	}

	this.LINEBREAK = function(){
		return [document.createElement("BR"), "linebreak"];			
	}

	this.INPUT = function(){
		return [document.createElement("INPUT"), "input"];
	}
	
	this.CLEAR = function(){
		document.body.innerHTML = "";
	}
}

function Structure() {
	this.model = new Rammy().LABEL()[0];
	this.element = new Rammy().LABEL()[1];

	this.compose = function(elem) {
		this.model = elem[0];
		this.element = elem[1];
	}
	
	this.setType = function(typeName) {
		this.model.setAttribute("type", typeName);
	}

	this.getType = function() {
		return this.model.type;
	}

	this.setEvent = function(method) {
		if(this.element == "button") {
			this.model.setAttribute("onClick", method);
		}
	}

	this.getEvent = function() {
		return this.model.onClick;
	}

	this.setId = function(id) {
		this.model.setAttribute("id", id);
	}
	
	this.getId = function() {
		return this.model.id;
	}

	this.setClass = function(className) {
		this.model.setAttribute("class", className);
	}

	this.getClass = function() {
		this.model.className;
	}

	this.setText = function(text) {
		this.model.innerHTML = text;
	}

	this.getText = function() {
		return this.model.innerHTML;
	}

	this.setValue = function(val) {
		this.model.value = val;
	}

	this.getValue = function() {
		return this.model.value;
	}

	this.setDisabled = function(bool) {
		this.model.disabled = bool;
	}

	this.getDisabled = function() {
		return this.model.disabled;
	}

	this.add = function() {
		document.body.appendChild(this.model);
	}
}
