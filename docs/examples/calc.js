var app = new Rammy();
app.START();
app.setTitle("Calculator");

function makeButton(text) {
	var button = new Structure();
	button.compose(app.BUTTON());
	button.setText(text);
	button.setEvent("callAction('" + text + "')");
	return button;
}

function lineBreak() {
	var br = new Structure();
	br.compose(app.LINEBREAK());
	return br;
}

function label() {
	var label = new Structure();
	label.compose(app.LABEL());
	return label;
}

function update() {
	var curr = 9;
	var coll = 3;
	var inputs = new Structure();
	inputs.compose(app.INPUT());
	inputs.setType("text");
	inputs.setId("inputs");
	inputs.add();
	lineBreak().add();
	var currLine = 1;
	while(curr > 0) {
		while(coll > 0 && curr >= 0) {
			makeButton(curr).add();
			coll = coll - 1;
			curr = curr - 1;
		}
		coll = 3;
		currLine = currLine + 1;
		if(currLine != 5) {
			lineBreak().add();
		}
	}
	makeButton("0").add()
	makeButton("C").add();
	makeButton("=").add();
	lineBreak().add();
	var operationLabel = label();
	operationLabel.setText("-- Operators --");
	operationLabel.add();
	lineBreak().add();
	makeButton("+").add();
	makeButton("-").add();
	makeButton("*").add();
	makeButton("/").add();
	lineBreak().add();
}

function callAction(thing) {
	var inputText = "12.34567890-+/*";
	var actionText = "C=";
	var box = document.getElementById("inputs");
	if(inputText.indexOf(thing) >= 0) {
		if(box.value == undefined) { box.value = ""; }
		box.value = box.value + thing;
	} else if(actionText.indexOf(thing) >= 0) {
		if(thing == "C") {
			box.value = "";
		} else if(thing == "=") {
			box.value = eval(box.value);
		}
	}
}

update();
